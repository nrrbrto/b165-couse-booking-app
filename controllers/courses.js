//[SECTION]	Dependencies and Modules
	const Course = require ('../models/Course')
	const auth = require("../auth")
	const {verify, verifyAdmin} = auth

//[SECTION] Funcionanality [CREATE]
	module.exports.createCourse = (info) => {
		let cName = info.name
		let cDesc = info.description
		let cCost = info.price
		let cisActive = info.isActive
		let newCourse = new Course({
			name: cName,
			description: cDesc,
			price: cCost,
			isActive: cisActive
		}) 
		
		return newCourse.save().then((savedCourse, error) =>{
			if (error) {
				return 'Failed to Save New Document'
			} else {
				return savedCourse;
			}
		})
	}

module.exports.getAllCourse = () => {
	return Course.find({}).then(outcome =>{
		return outcome
	}) 
	}

module.exports.getCourse = (id) => {
	return Course.findById(id).then(resultOfQuery =>{
	return resultOfQuery
	})
}

module.exports.getAllActiveCourse = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//[SECTION] Funcionanality [UPDATE]
module.exports.updateCourse = (id, details) => {
   let cName = details.name;
   let cDesc = details.description;
   let cPrice = details.price;

   let updatedCourse = {
   	name: cName,
    	description: cDesc,
    	price: cPrice
   };
   return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
    	if (err) {
    		return 'Failed to update Course';
    	} else {
    		return 'Successfully Updated Course';
    	}
   })
   }

module.exports.deactivateCourse = (id) => {
	let updates = {
		isActive: false
	}
	return Course.findByIdAndUpdate(id,updates).then((archived, err) =>{
		if (archived) {
			return `The course with ID:${id} has been deactivated`
		} else {
			return 'Failed to archive course'
		}
	})
}

module.exports.reactivateCourse = (id) => {
	let updates = {
		isActive: true
	}
	return Course.findByIdAndUpdate(id,updates).then((reactivate, err) =>{
		if (reactivate) {
			return `The course with ID:${id} has been reactivated`
		} else {
			return 'Failed to reactivate course'
		}
	})
}

//[SECTION] Funcionanality [DELETE]
module.exports.deleteCourse = (id) => {
	return Course.findByIdAndRemove(id).then((removedCourse, err) => {
		if (err) {
			return 'No Course Was Removed'
		} else {
			return `The course '${removedCourse.name}' Was Successfully Deleted` 
		}
	})
}
