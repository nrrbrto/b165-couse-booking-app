// [SECTION] Dependecies and Modules
	const exp = require("express")
	const controller = require("../controllers/courses.js")
	const auth = require("../auth")
	const {verify, verifyAdmin} = auth
// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] [POST] Routes 
	route.post('/create', (req,res) => {
		let data = req.body
		controller.createCourse(data).then(outcome => {
			res.send(outcome)
		})
	})

	route.get('/all', verify, verifyAdmin, (req,res) =>{
		controller.getAllCourse().then(outcome =>{
			res.send(outcome)
		})
	})

	route.get('/:id', (req,res) => {
		let courseId= req.params.id;
		controller.getCourse(courseId).then(result => {
			res.send(result)
		});
	})

	route.get('/', (req,res) => {
		controller.getAllActiveCourse().then(outcome => {
			res.send(outcome)
		})
	})

// [SECTION] [PUT] Routes
	route.put('/:id', (req, res) => {
	    let id = req.params.id;
	    let details = req.body;
	    let cName = details.name;
	    let cDesc = details.description;
	    let cPrice = details.price;
	    if (cName !== '' && cDesc !== '' && cPrice !== '') {
	    	controller.updateCourse(id, details).then(outcome => {
	    		res.send(outcome)
	    })
	    } else {
	    	res.send('Make sure all details are complete')
	    }
  }); 

	route.put('/:id/archive', (req,res) =>{
		let courseId = req.params.id
		controller.deactivateCourse(courseId).then(result => {
			res.send(result)
		});
	})

	route.put('/:id/reactivate', (req,res) =>{
		let courseId = req.params.id
		controller.reactivateCourse(courseId).then(result => {
			res.send(result)
		});
	})

// [SECTION] [DEL] Routes
	route.delete('/:id', (req,res)=> {
		let id = req.params.id;
		controller.deleteCourse(id).then(outcome =>{
			res.send(outcome)
		})
	}) 

// [SECTION] Export Route System
	module.exports = route;