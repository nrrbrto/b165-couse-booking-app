//[SECTION] Dependencies and Modules
	const exp = require('express')
	const controller = require("../controllers/users.js")
	const auth = require("../auth")
	const {verify, verifyAdmin} = auth
	
//[SECTION] Routing Component
	const route = exp.Router()

//[SECTION] Routes [POST]
	route.post('/register', (req,res) => {
		let userDetails = req.body
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome)
		})
	})

	route.post('/login', controller.loginUser)

//[SECTION] Routes [GET]

	route.get("/getUserDetails", verify, controller.getUserDetails)

  route.post('/enroll', verify, controller.enroll)
  
	route.get('/getEnrollments', verify, controller.getEnrollments)


//[SECTION] Expose Route System
 module.exports = route;